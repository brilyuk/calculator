import Modernizr from 'modernizr';

$ ->
	$('.slider-currency').ionRangeSlider
		min: 1000
		max: 100000
		from: 25000
		prefix: '$'

	$('.slider-period').ionRangeSlider
		min: 3
		max: 36
		from: 8
		postfix: ' мес'

	


	
	
	

	$(".slider-currency").on "change", ->
		currency = $(".slider-currency").val()
		percentRate = 18
		summ1 = currency * percentRate / 100 / 12
		$("#summ1").text("$"+ summ1.toFixed(2))
		console.log summ1
	
	$(".slider-period").on "change", ->
		currency = $(".slider-currency").val()
		period = $(".slider-period").val()
		percentRate = 18
		summ2 = currency * percentRate / 100 / 12 * period
		$("#summ2").text("$"+ summ2.toFixed(2))
		console.log summ2
		